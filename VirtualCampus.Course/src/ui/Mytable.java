package ui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import common.ButtonEditor;
import common.ButtonRender;

//这个类只是设置了一些表格的属性。
public class Mytable extends JTable{
	
	public Mytable() {
		
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setRowHeight(40);

		this.setCellSelectionEnabled(true);
		setOpaque(false);

		
	}

	

	//设置第9列（即按钮所在列）为可编辑，这样才能实现表格中按钮的动作监听
		public boolean isCellEditable(int row, int col) {
			if (col == 8) {
				return true;
			} else {
				return false;
			}
		}
		
		public Component prepareRenderer(TableCellRenderer renderer,
			     int row, int column) {
			    Component c = super.prepareRenderer(renderer, row, column);
			    if (c instanceof JComponent) {
			     ((JComponent) c).setOpaque(false);
			    }
			    return c;
			   }
}
