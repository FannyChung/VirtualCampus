package ui;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import database.DataBase;
//这个类实现了单击“查看课表”按钮时出现的对话框
public class Dialog extends JDialog {
	String userID;
	Object[] names = { "课程名称", "学分", "上课安排", "授课教师", "考核方式", "说明" };//表头
	Object[] row = new Object[6];//该表有6列，每行为一组数据
	Object[][] data;	//空数据，声明时用
	DefaultTableModel tableModel = new DefaultTableModel(data, names);//声明一个TableModel
	Mytable table;
	public Dialog(String ID) {
		userID = ID;//获取当前用户ID（一卡通号）
		DataBase db1 = new DataBase();//新建一个数据库类，通过此类或许数据或修改数据库
		
		String queryString = "select * from xk_Student where 一卡通号='" + userID
				+ "'";//通过一卡通号在表中选中当前用户
		ResultSet rss = db1.getResultSet(queryString);
		try {
			while (rss.next()) {
				String courseID = rss.getString(2);//获取当前用户所选课程的课程编号
				String query = "select * from xk_Course where 课程编号='"
						+ courseID + "'";//通过课程编号获取该课程的完整信息
				DataBase db2 = new DataBase();//新建一个数据库类，通过此类或许数据或修改数据库
				ResultSet rss2 = db2.getResultSet(query);//通过课程编号获取该课程的完整信息
				//将获得的课程信息赋到row数组中去
				while(rss2.next())
				{
				row[0] = rss2.getString(3);
				row[1] = rss2.getInt(4);
				row[2] = rss2.getString(5);
				row[3] = rss2.getString(6);
				row[4] = rss2.getString(7);
				row[5] = rss2.getString(8);
				
				tableModel.addRow(row);//向tableModel增加一行，改行为刚获取的课程数据
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		table = new Mytable();//声明一个新表
		table.setModel(tableModel);
		initLayout();//这是设置该对话框的布局的函数，详见下方
		setColumnWidth();//这是调整表格中某些列列宽的函数，详见下方
		
	}
	
	//这是设置该对话框的布局的函数
	public void initLayout()
	{
		ImageIcon img = new ImageIcon(getClass().getResource(
				"/res/xk-view-bg.png"));
		JLabel background = new JLabel(img);
		getLayeredPane().add(background, new Integer(Integer.MIN_VALUE));
		background.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());
		setTitle("当前课表");
		setBounds(0, 0, 1000, 700);
		JPanel jp = new JPanel();
		jp.setOpaque(false);
		getContentPane().add(jp);
		((JPanel)getContentPane()).setOpaque(false);
		jp.setLayout(null);
		JScrollPane sp = new JScrollPane(table);
		jp.add(sp);
		sp.setBounds(0, 115, 1000, 700);
		sp.setOpaque(false);
		sp.getViewport().setOpaque(false);
		setVisible(true);
	}
	
	//这是调整表格中某些列列宽的函数
	public void setColumnWidth() {
		TableColumn column = table.getColumnModel().getColumn(0);
		column.setPreferredWidth(180);
		column = table.getColumnModel().getColumn(2);
		column.setPreferredWidth(520);
	}
}
