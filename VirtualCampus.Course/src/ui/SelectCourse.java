package ui;

import javax.swing.*;
import javax.swing.table.TableColumn;

import common.ButtonEditor;
import common.ButtonRender;
import common.ClientLoginWindow;
import common.Model;

import database.DataBase;
import common.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;

public class SelectCourse extends JFrame {
	
	private static final String JLabel = null;
	public static String UserID;         //用户ID，一卡通号
	public Mytable table;
	public JButton queryCourse;
	public SelectCourse(ClientLoginWindow window) {
		//从登陆窗口获取一卡通号
		UserID = window.getTextUserID().getText();

		table = new Mytable();
		Model myModel = new Model(UserID);
		table.setModel(myModel.getTableModel());
		initLayout();//这是设置布局的函数，实现在下方
		setColumnWidth();//这是调整表格中某些列列宽的函数，实现在下方
		//这里实现按钮“查询课表”的监听，该按钮的实现在initLayout函数中
		queryCourse.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new Dialog(UserID);//弹出一个对话框，显示当前课表，该对话框类在ui包中实现
				}
		});
//		table.setOpaque(true);
		//自定义表格中特定一列的渲染器，为了在表格中显示按钮
		ButtonRender myRender = new ButtonRender();
		table.getColumn("操作").setCellRenderer(myRender);
		//自定义表格中特定一列的cellEditor，为了实现按钮监听
		table.getColumn("操作").setCellEditor(
				new ButtonEditor(table, myRender, myModel));
	}

	//这是调整表格中某些列列宽的函数
	public void setColumnWidth() {
		TableColumn column = table.getColumnModel().getColumn(0);
		column.setPreferredWidth(120);
		column = table.getColumnModel().getColumn(8);
		column.setPreferredWidth(120);
		column = table.getColumnModel().getColumn(2);
		column.setPreferredWidth(150);
		column = table.getColumnModel().getColumn(4);
		column.setPreferredWidth(520);
	}

	//这是设置布局的函数
	public void initLayout() {
		ImageIcon img = new ImageIcon(getClass().getResource(
				"/res/xkbg02.jpg"));
		JLabel background = new JLabel(img);
		getLayeredPane().add(background, new Integer(Integer.MIN_VALUE));
		background.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());
		
		setTitle("选课系统");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1366, 768);
		setResizable(true);
		setLocationRelativeTo(getOwner());
		JPanel jp = new JPanel();
		jp.setOpaque(false);
		
		getContentPane().add(jp);
		((JPanel)getContentPane()).setOpaque(false);
		jp.setLayout(null);
		JScrollPane sp = new JScrollPane(table);
		jp.add(sp);
		sp.setBounds(0, 150, 1366, 768);
		sp.setOpaque(false);
		sp.getViewport().setOpaque(false);
		queryCourse = new JButton("查询课表");
		queryCourse.setBorderPainted(false);
		queryCourse.setIcon(new ImageIcon(getClass().getResource("/res/xk-view.png")));
		queryCourse.setPressedIcon(new ImageIcon("/res/xk-view-pressed.png"));
		queryCourse.setHorizontalTextPosition(0);
		jp.add(queryCourse);
		queryCourse.setBounds(600, 85, 166, 35);
		
		//显示当前用户ID的标签
		JLabel currentUser = new JLabel();
		currentUser.setText("当前用户： "+UserID);
		currentUser.setBounds(20, 100, 200, 40);
		jp.add(currentUser);
		
	}
}
