package common;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;

import javax.swing.*;
import javax.swing.table.*;

import ui.SelectCourse;
import database.DataBase;
//这是实现JTable时要用的tableModel
public class Model extends DefaultTableModel {
	Object[] names = { "课程类别", "课程编号", "课程名称", "学分", "上课安排", "授课教师", "考核方式",
			"说明", "操作", "当前状态" };//表头
	Object[] row = new Object[11];//每一行为一个数据组，共有11列。
	Object[][] data;//空data，仅在声明tableModel时用
	DefaultTableModel tableModel = new DefaultTableModel(data, names);
	String userID;//用户ID（一卡通号）
	

	public Model(String ID) {
		userID = ID;
		//声明数据库类，从数据库中获取课程信息。
		DataBase db = new DataBase();
		String query = "select * from xk_Course";
		ResultSet rss = db.getResultSet(query);
		DataBase db2 = new DataBase();
		try {
			while (rss.next()) {
				row[0] = rss.getString(1);
				row[1] = rss.getString(2);
				row[2] = rss.getString(3);
				row[3] = rss.getInt(4);
				row[4] = rss.getString(5);
				row[5] = rss.getString(6);
				row[6] = rss.getString(7);
				row[7] = rss.getString(8);
				
				//下面通过查表判断当前课程是否已选，设置初始操作按钮名字和初始状态
				String query2 = "select * from xk_Student where 一卡通号='"
						+ userID + "'and 课程编号=" + row[1];
				ResultSet rss2 = db2.getResultSet(query2);
				//如果有数据返回，则初始按钮为“退选”，初始状态为“已选”
				if (rss2.next()) {
					row[8] = "退选";
					row[9] = "已选";
				} 
				//否则初始按钮为“选择”，初始状态为“未选”
				else {
					row[8] = "选择";
					row[9] = "未选";
				}
				tableModel.addRow(row);//向tableModel 中增加一行数据
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//下列函数未注释的函数都不太重要
	public DefaultTableModel getTableModel() {
		return tableModel;
	}

	public void setTableModel(DefaultTableModel tableModel) {
		this.tableModel = tableModel;
	}

	public Object[] getNames() {
		return names;
	}

	public void setNames(Object[] names) {
		this.names = names;
	}

	public int getColumnCount() {
		return names.length;
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public String getColumnName(int colIndex) {
		return names[colIndex].toString();
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}
	

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}
}
