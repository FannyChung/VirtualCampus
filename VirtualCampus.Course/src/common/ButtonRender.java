package common;
import java.awt.Component;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

//这是自定义的渲染器类
public class ButtonRender implements TableCellRenderer {
	private JPanel panel;
	private JButton button;
	
	public ButtonRender() {
		this.button = new JButton();
		this.button.setBorderPainted(false);
		this.button.setIcon(new ImageIcon(getClass().getResource("/res/btn11.jpg")));
		this.button.setHorizontalTextPosition(0);
		// 设置按钮的大小及位置。
		this.button.setBounds(12, 5, 100, 30);
		this.panel = new JPanel();
		// panel使用绝对定位，这样button就不会充满整个单元格。
		this.panel.setLayout(null);
		// 添加按钮。
		this.panel.add(this.button);
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JButton getButton() {
		return button;
	}

	public void setButton(JButton button) {
		this.button = button;
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		// 只为按钮赋值即可。也可以作其它操作，如绘背景等。
		this.button.setText(value == null ? "" : String.valueOf(value));
		
		return this.panel;
	}
}
