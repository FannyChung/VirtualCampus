package common;

import java.awt.Button;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;  

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import database.DataBase;

public class ButtonEditor extends DefaultCellEditor  
{  
  
    /** 
     * serialVersionUID 
     */  
    private static final long serialVersionUID = -6546334664166791132L;  
    private JPanel panel;  
    private JButton button;  
    private JTable fulltable;//这是要传入的整张表
    String userID;
    public ButtonEditor(JTable table,ButtonRender render,Model myModel)  
    {  
        // DefautlCellEditor有此构造器，需要传入一个，但这个不会使用到，直接new一个即可。   
        super(new JTextField());  
  
        // 设置点击几次激活编辑。   
        this.setClickCountToStart(0);  
        this.button = render.getButton();
        //传入整张表格
        this.fulltable = table;
        userID = myModel.getUserID();
        this.initButton();  
        
        this.panel = render.getPanel();
        // panel使用绝对定位，这样button就不会充满整个单元格。   
        this.panel.setLayout(null); 
        // 添加按钮。   
        this.panel.add(this.button);  
    }  
  
    private void initButton()  
    {  
        // 设置按钮的大小及位置。   
//        this.button.setBounds(0, 0, 50,50);  
  
        // 为按钮添加事件。这里只能添加ActionListner事件，Mouse事件无效。   
        this.button.addActionListener(new ActionListener()  
        {  
            public void actionPerformed(ActionEvent e)  
            {  

            	DataBase db = new DataBase();//声明一个数据库类，通过此类获取数据库信息或修改数据库
            	int currentRow = fulltable.getSelectedRow();//获取当前行行号
            	String currentCourse = (String) fulltable.getValueAt(currentRow, 1);//获取当前行对应的课程编号
            	//如果当前课程状态为“已选”，此时按钮名为“退选”，那么单击时状态变为“未选”，按钮名称变为“选择”
            	//同时修改数据库，向xk_Student增加一条记录，该记录包含一卡通号和课程编号。
            	if (fulltable.getValueAt(currentRow, 9)=="已选") {
					
					fulltable.setValueAt("未选", currentRow, 9);
//					button.setIcon(new ImageIcon(getClass().getResource("/res/btn11.jpg")));
					button.setText("选择");
					fireEditingStopped();
					String update = "delete from xk_Student where 一卡通号='"+userID+"' and 课程编号='"+currentCourse+"'";
			    	db.update(update);
				}
            	//否则的话，就是当前课程状态为“未选”，此时按钮名为“选择”，那么单击时状态变为“已选”，按钮名称变为“退选”
            	//同时修改数据库，根据一卡通号和课程编号，从xk_Student删除一条记录。
            	else {
            		fulltable.setValueAt("已选", currentRow, 9);
//            		button.setIcon(new ImageIcon(getClass().getResource("/res/btn22.jpg")));
            		button.setText("退选");
            		fireEditingStopped();
            		String update = "insert into xk_Student values ('"+userID+"', '"+currentCourse+"')";
			    	db.update(update);
				}
            	
            	 
            }  
        });  
  
    }  
  
   
//    public boolean stopCellEditing() {
//      //  isPushed = false;
//        return super.stopCellEditing();
//      }
//
//      protected void fireEditingStopped() {
//        super.fireEditingStopped();
//      }
    /** 
     * 这里重写父类的编辑方法，返回一个JPanel对象即可（也可以直接返回一个Button对象，但是那样会填充满整个单元格） 
     */  
    @Override  
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)  
    {  
        // 只为按钮赋值即可。也可以作其它操作。   
        this.button.setText(value == null ? "" : String.valueOf(value));  
  
        return this.panel;  
    }  
  
    /** 
     * 重写编辑单元格时获取的值。如果不重写，这里可能会为按钮设置错误的值。 
     */  
    @Override  
    public Object getCellEditorValue()  
    {  
        return this.button.getText();  
    }  
  
} 