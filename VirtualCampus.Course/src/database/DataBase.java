package database;

import java.sql.*;

//该数据库类集合了选课中所有的数据库操作。
public class DataBase {
	private static Connection cnn = null;
	private static Statement stm;
	private static ResultSet rss;

	public DataBase() {
		// 建立连接
		try {
			Class.forName("com.mysql.jdbc.Driver");
			cnn = DriverManager
					.getConnection(
							"jdbc:mysql://121.248.63.106:3306/xindervella_VirtualCampus",
							"xindervella", "hu@idi@nn@0");
			stm = cnn.createStatement();

		} catch (ClassNotFoundException cnfex) {
			System.err.println("装载 JDBC/ODBC 驱动程序失败。");
			cnfex.printStackTrace();
			System.exit(1); // terminate program
		} catch (SQLException sqlex) {
			System.err.println("无法连接数据库");
			sqlex.printStackTrace();
			System.exit(1); // terminate program
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	//获取ResultSet
	public ResultSet getResultSet(String query) {
		try {
			rss = stm.executeQuery(query);
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return rss;

	}

	//修改数据库内容
	public void update(String update) {
		try {
			stm.executeUpdate(update);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
