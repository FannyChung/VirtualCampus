package LibraryManager;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;



public class OperateDB {
	public static String DRIVER_NAME = "com.mysql.jdbc.Driver";
	public static String CONN_URL = "jdbc:mysql://121.248.63.106:3306/xindervella_VirtualCampus";
	public static String USER_NAME = "xindervella";
	public static String PASSWORD = "hu@idi@nn@0";
	public Statement connStat;
	protected Connection conn;
	
	
	/**
	 * 杈撳嚭鏁版嵁搴撲腑鎵�湁鐢ㄦ埛
	 * @throws SQLException
	 */
	public void printAllUserInDB() throws SQLException{
		//鍦ㄧ粓绔墦鍗板嚭鐩稿簲缁撴灉
		
		String sqlPrintUser;	//鍌ㄥ瓨sql璇彞
		try {
			Class.forName(DRIVER_NAME);	//鍔犺浇 mysql jdbc
			conn = DriverManager.getConnection(CONN_URL, USER_NAME, PASSWORD); //閾炬帴杩滅▼鏁版嵁搴�
			

			Statement stmt = conn.createStatement();  //鐢ㄤ簬鍦ㄥ凡缁忓缓绔嬮摼鎺ョ殑鍩虹涓婂悜鏁版嵁搴撳彂閫佽鎵ц鐨剆ql璇彞
			sqlPrintUser = "SELECT * FROM vcUser";   //瑕佹墽琛岀殑sql璇彞 
			// 甯哥敤sql璇彞 http://www.cnblogs.com/yubinfeng/archive/2010/11/02/1867386.html
			ResultSet rsPrintUser = stmt.executeQuery(sqlPrintUser);  //閫氳繃鎵цsql璇彞鐢熸垚鏁版嵁搴撶粨鏋滈泦鐨勬暟鎹〃
			
			
			while(rsPrintUser.next()){
				// 鎵撳嵃鍑�rsUser缁撴灉闆�涓殑鍏ㄩ儴鐢ㄦ埛淇℃伅
				System.out.println("涓�崱閫氾細 " + rsPrintUser.getString("uBookName") + "\t" 
									+ "瀵嗙爜锛�" + rsPrintUser.getString("uWriter") + "\t"
									+ "韬唤锛�" + rsPrintUser.getString("uNo"));
			}
			
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			// 鍏抽棴涓庢暟鎹簱閾炬帴 锛侊紒 蹇呴』瑕佸啓锛侊紒
			conn.close();
		}
	}
	
	/**
	 * 鍦ㄦ暟鎹簱涓彃鍏ユ柊鐨勭敤鎴蜂俊鎭�
	 * @param userID
	 * @param password
	 * @param role
	 * @throws SQLException
	 */
	public void insertUserToDB(String uBookName, String uWriter, String uNo) throws SQLException{
		//鍚戞暟鎹簱涓鍔犳柊鐢ㄦ埛
		
		String sqlInsertUser;
		try{
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(CONN_URL, USER_NAME, PASSWORD);
			
			Statement stmt = conn.createStatement();
			sqlInsertUser = "INSERT INTO `xindervella_VirtualCampus`.`vcBook` (`uBookName`, `uWriter`, `uNo`) VALUES ('"+uBookName+"', '"+uWriter+"', '"+uNo+"')";//, userID, password, role);
			//濡傛灉浼犻�杩涙潵鍙傛暟涓� insertUserToDB( "213110561", "11111", "student")
			// sqlInsertUser 灏辨槸 "INSERT INTO `xindervella_VirtualCampus`.`vcUser` (`uID`, `uPwd`, `uRole`) VALUES ('213110561', '1111', 'student')"
			stmt.executeUpdate(sqlInsertUser);	//鐩存帴鎵цsql璇彞  鍒涘缓鏂扮敤鎴蜂笉闇�鑾峰彇淇℃伅 鎵�互涓嶉渶瑕佺敓鎴愮粨鏋滄暟鎹泦
		
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
	
	 public Vector<Vector<String>> GetBookInfo() throws SQLException{			
   	  String sqlBook;
   	  Vector<Vector<String>> result = new Vector<Vector<String>>();
  
   	  //储存sql语句		
   	  try 
   	  {		
   		  Class.forName(DRIVER_NAME);	//加载 mysql jdbc	
   		  conn = DriverManager.getConnection(CONN_URL, USER_NAME, PASSWORD); //链接远程数据库			
   		  Statement stmt = conn.createStatement();  //用于在已经建立链接的基础上向数据库发送要执行的sql语句	
   		  sqlBook = "SELECT * FROM vcBook ";   //要执行的sql语句 
   		  // 常用sql语句 http://www.cnblogs.com/yubinfeng/archive/2010/11/02/1867386.html	
   		  ResultSet rsBook = stmt.executeQuery(sqlBook);  //通过执行sql语句生成数据库结果集的数据表	
   		  while(rsBook.next())
   		  {
   			  Vector<String> book = new Vector();
   			  book.add(rsBook.getString(1));
   			  book.add(rsBook.getString(2));
   			  book.add(rsBook.getString(3));
   			  book.add(rsBook.getString(10));
   			  book.add(rsBook.getString(4));
   			  book.add(rsBook.getString(7));
   			  book.add(rsBook.getString(5));
   			  book.add(rsBook.getString(6));
   			  book.add(rsBook.getString(8));
   			  book.add(rsBook.getString(9));
   			  result.add(book);
   		  }
   		  
   	  } catch (Exception e){	
   			  e.printStackTrace();	
   			  } finally {	
   				  // 关闭与数据库链接 ！！ 必须要写！！	
   				  conn.close();		
   				  }	
   	  
   	  return result;
   	  }
	 
	 public void DelBookInfo(String uBookName) throws SQLException{
			
			String sql;
			try{
				Class.forName(DRIVER_NAME);
				conn = DriverManager.getConnection(CONN_URL, USER_NAME, PASSWORD);
				
				Statement stmt = conn.createStatement();
				
				sql = "DELETE FROM `xindervella_VirtualCampus`.`vcBook`  WHERE uBookName='"+uBookName+"'" ;
				
				stmt.executeUpdate(sql);	
				
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				conn.close();
			}
		}
	 
	 public Vector <Vector<String>> Search(String uBookName ,String uWriter ,String uNo) throws SQLException{			
   	  String sqlBook = "SELECT * FROM vcBook WHERE 1=1";
   	  Vector<Vector<String>> result = new Vector<Vector<String>>();
   	  
   	  //储存sql语句		
   	  try 
   	  {		
   		  Class.forName(DRIVER_NAME);	//加载 mysql jdbc	
   		  conn = DriverManager.getConnection(CONN_URL, USER_NAME, PASSWORD); //链接远程数据库			
   		  Statement stmt = conn.createStatement();  //用于在已经建立链接的基础上向数据库发送要执行的sql语句	
   		  if(!uBookName.isEmpty())
   		  {
   			  sqlBook = sqlBook + " AND uBookName LIKE '%"+uBookName+"%'";
   		  }
   		  
   		  if(!uWriter.isEmpty())
   		  {
   			sqlBook = sqlBook + " AND uWriter LIKE '%"+uWriter+"%'";
   		  }
   	  
   		  if(!uNo.isEmpty())
   		  {
   			sqlBook = sqlBook + " AND uNo LIKE '%"+uNo+"%'";
   		  }
   	  
   		  // 常用sql语句 http://www.cnblogs.com/yubinfeng/archive/2010/11/02/1867386.html	
   		  ResultSet rsBook = stmt.executeQuery(sqlBook);  //通过执行sql语句生成数据库结果集的数据表	
   		  while(rsBook.next())
   		  {
   			  Vector<String> book = new Vector();
   			  book.add(rsBook.getString(1));
 			  book.add(rsBook.getString(2));
 			  book.add(rsBook.getString(3));
 			  book.add(rsBook.getString(7));
 			  book.add(rsBook.getString(6));
 			  book.add(rsBook.getString(8));
 			  book.add(rsBook.getString(9));
 			  result.add(book);
   		  }
   		  
   	  } catch (Exception e){	
   			  e.printStackTrace();	
   			  } finally {	
   				  // 关闭与数据库链接 ！！ 必须要写！！	
   				  conn.close();		
   				  }	
   	  
   	  return result;
   	  }
	 
	 public boolean BorrowBook(String uBookName ,String uStudentNo,String BorrowDate) throws SQLException{
			
			String sql;
			String sql1;
			String sql2;
			String sql3;
			String sql4;
			String name = null;
			boolean flag = false;
			try{
				Class.forName(DRIVER_NAME);
				conn = DriverManager.getConnection(CONN_URL, USER_NAME, PASSWORD);
				Statement stmt = conn.createStatement();
				Statement stmt1 = conn.createStatement();
				Statement stmt2 = conn.createStatement();
				Statement stmt3 = conn.createStatement();
				Statement stmt4 = conn.createStatement();
				sql = "UPDATE `xindervella_VirtualCampus`.`vcBook` SET uBorrowState = 'y' WHERE uBookName='"+uBookName+"'" ;
				sql1 = "UPDATE `xindervella_VirtualCampus`.`vcBook` SET uStudentNo ='"+uStudentNo+"' WHERE uBookName='"+uBookName+"'" ;
				sql3 = "SELECT * FROM vcStudent WHERE sCard ='"+uStudentNo+"'";
				sql4 = "UPDATE `xindervella_VirtualCampus`.`vcBook` SET uBorrowDate ='"+BorrowDate+"' WHERE uBookName='"+uBookName+"'" ;
				ResultSet rs = stmt.executeQuery(sql3); 
				while(rs.next())
				{
					name=rs.getString(3);
					sql2 = "UPDATE `xindervella_VirtualCampus`.`vcBook` SET uBorrowName ='"+name+"' WHERE uBookName='"+uBookName+"'" ;
					stmt1.executeUpdate(sql);
					stmt2.executeUpdate(sql1);
					stmt3.executeUpdate(sql2);
					stmt4.executeUpdate(sql4);
				}
				
				
				
				flag = true;
				
				
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				conn.close();
			}
			
			return flag ;
		}
	 
	 public Vector<Vector<String>> GetBorrowBook(String uStudentNo) throws SQLException{			
	   	  String sqlBook;
	   	  Vector<Vector<String>> result = new Vector<Vector<String>>();
	  
	   	  //储存sql语句		
	   	  try 
	   	  {		
	   		  Class.forName(DRIVER_NAME);	//加载 mysql jdbc	
	   		  conn = DriverManager.getConnection(CONN_URL, USER_NAME, PASSWORD); //链接远程数据库			
	   		  Statement stmt = conn.createStatement();  //用于在已经建立链接的基础上向数据库发送要执行的sql语句	
	   		  sqlBook = "SELECT * FROM vcBook WHERE uStudentNo='"+uStudentNo+"'"+" AND uBorrowState = 'y'";   //要执行的sql语句 
	   		  // 常用sql语句 http://www.cnblogs.com/yubinfeng/archive/2010/11/02/1867386.html	
	   		  ResultSet rsBook = stmt.executeQuery(sqlBook);  //通过执行sql语句生成数据库结果集的数据表	
	   		  while(rsBook.next())
	   		  {
	   			  Vector<String> book = new Vector();
	   			  book.add(rsBook.getString(1));
	   			  book.add(rsBook.getString(2));
	   			  book.add(rsBook.getString(3));
	   			  book.add(rsBook.getString(7));
	   			  book.add(rsBook.getString(6));
	   			  book.add(rsBook.getString(8));
	   			  book.add(rsBook.getString(9));
	   			  result.add(book);
	   		  }
	   		  
	   	  } catch (Exception e){	
	   			  e.printStackTrace();	
	   			  } finally {	
	   				  // 关闭与数据库链接 ！！ 必须要写！！	
	   				  conn.close();		
	   				  }	
	   	  
	   	  return result;
	   	  }
	 
	 public boolean ReturnBook(String uBookName) throws SQLException{
			
			String sql;
			String sql1;
			String sql2;
			String sql3;
			boolean flag = false;
			try{
				Class.forName(DRIVER_NAME);
				conn = DriverManager.getConnection(CONN_URL, USER_NAME, PASSWORD);
				Statement stmt1 = conn.createStatement();
				Statement stmt2 = conn.createStatement();
				Statement stmt3 = conn.createStatement();
				Statement stmt4 = conn.createStatement();
				sql = "UPDATE `xindervella_VirtualCampus`.`vcBook` SET uBorrowState = '' WHERE uBookName='"+uBookName+"'" ;
				sql1 = "UPDATE `xindervella_VirtualCampus`.`vcBook` SET uStudentNo ='' WHERE uBookName='"+uBookName+"'" ;
				sql2 = "UPDATE `xindervella_VirtualCampus`.`vcBook` SET uBorrowName ='' WHERE uBookName='"+uBookName+"'" ;
				sql3 = "UPDATE `xindervella_VirtualCampus`.`vcBook` SET uBorrowDate ='' WHERE uBookName='"+uBookName+"'" ;
				
					stmt1.executeUpdate(sql);
					stmt2.executeUpdate(sql1);
					stmt3.executeUpdate(sql2);
					stmt4.executeUpdate(sql3);
				
				
				
				
				flag = true;
				
				
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				conn.close();
			}
			
			return flag ;
		}
	 
}
