package LibraryManager;

import java.awt.AWTEvent;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
/** * @author C5102 * * TODO To change the template for this generated type comment go to * Window - Preferences - Java - Code Style - Code Templates */

public class ReturnBook extends JFrame{	
	/**	 * Comment for <code>serialVersionUID</code>	 */	
	private JPanel contentPanel;	
	private MainWindow mw;
	private static MainWindow window;
	OperateDB dbConnMng = new OperateDB();
	String uStudentNo = "213112403";
	String uBorrowState;
	String uBookName;
	/**	 * @param window	 */	
	
	Vector<Vector<String>> rowData = new Vector<Vector<String>>();
	 Vector columnData = new Vector();
	 
	 
	public ReturnBook(MainWindow mwIn) {	
		this.mw = mwIn;
		
		
	    
		setResizable(false);
		setTitle("图书馆管理系统");	
		setBounds(100, 100, 800, 550);
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		setLocationRelativeTo(getOwner());	
		setType(Type.POPUP);
		
		JPanel jp = new JPanel();	
	    jp.setOpaque(false);		
	    getContentPane().add(jp);	
	    ((JPanel) getContentPane()).setOpaque(false);	
	   // ImageIcon img = new ImageIcon(getClass().getResource("/res/mainwindow.png"));	
	    //JLabel background = new JLabel(img);	
	    //getLayeredPane().add(background, new Integer(Integer.MIN_VALUE));	
	   // background.setBounds(-75, -15, img.getIconWidth(), img.getIconHeight());
	    jp.setLayout(null);
	    
	    columnData.add("书名");
	    columnData.add("作者");
	    columnData.add("编号");
	    columnData.add("借书状态");
	    columnData.add("预约状态");
	    columnData.add("借书时间");
	    columnData.add("归还时间");
	    try {	
			
			rowData.addAll(dbConnMng.GetBorrowBook(uStudentNo));
				
	 } catch (SQLException e) {
					// TODO Auto-generated catch block
		e.printStackTrace();
				}
	
	    
	    final JTable table = new JTable(rowData, columnData);
		final JScrollPane scrollpane = new JScrollPane(table);
		scrollpane.setBounds(70, 140, 680, 350); 
		table.setRowHeight(50); 
	    table.setPreferredScrollableViewportSize(new Dimension(100,100));
	    jp.add(scrollpane);
	    //JButton btnInfoMng = new JButton("学生信息管理");
	    //btnInfoMng.setBorderPainted(true);
	    //btnInfoMng.setBounds(600, 70, 112, 40);
	    //jp.add(btnInfoMng);
	    
	    JButton btnReturn = new JButton("归还图书");
	    btnReturn.setBorderPainted(true);
	    btnReturn.setBounds(600, 30, 140, 40);
	    btnReturn.addMouseListener(new MouseAdapter() 
	    {		
	    	@Override		
	    	public void mouseClicked(MouseEvent arg0) 
	    	{		
	    		int row = table.getSelectedRow();		
	    		if(row == -1)
	    		{					
	    			JOptionPane.showMessageDialog(null,"请选择要归还的书!");	
	    		}
	    		
    		
	    		else
	    		{
	    			try {
	    				    uBookName = (String) table.getValueAt(row, 0);
	    					if(dbConnMng.ReturnBook(uBookName)==true)
	    					{
	    						JOptionPane.showMessageDialog(null,"还书成功！");	
	    					}
	    					else 
	    						JOptionPane.showMessageDialog(null,"还书失败!");	
	    					
					} catch (HeadlessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    		}
	    		
	    	}		
		
	    });	
	    jp.add(btnReturn);
	    
	    //JButton btnStatistic = new JButton("取消预约");
	    //btnStatistic.setBorderPainted(true);
	    //btnStatistic.setBounds(600, 70, 140, 40);
	    //jp.add(btnStatistic);
	    
	    //JButton btnHelp = new JButton("归还");
	    //btnHelp.setBorderPainted(true);
	    //btnHelp.setBounds(600, 300, 140, 40);	
	    //jp.add(btnHelp);
	    
	    JLabel label1 = new JLabel();
	    label1.setText("姓名    ：王麻子");
	    label1.setBounds(80, 30, 200, 40);
	    jp.add(label1);
	    
	    JLabel label2 = new JLabel();
	    label2.setText("学号    ：9999");
	    label2.setBounds(80, 50, 200, 40);
	    jp.add(label2);
	    
	    JLabel label3 = new JLabel();
	    label3.setText("一卡通：213119999");
	    label3.setBounds(80, 70, 200, 40);
	    jp.add(label3);
	    
	    
		//clw.frame.setVisible(false);	
		}
	}