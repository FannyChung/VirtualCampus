package LibraryManager;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
/** * @author C5102 * * TODO To change the template for this generated type comment go to * Window - Preferences - Java - Code Style - Code Templates */

public class Data extends JFrame{	
	protected static final AbstractButton Iabel3 = null;
	/**	 * Comment for <code>serialVersionUID</code>	 */	
	private JPanel contentPanel;	
	private Manager mw;
	private static Manager window;
	OperateDB dbConnMng = new OperateDB();
	 Vector<Vector<String>> rowData = new Vector<Vector<String>>();
	  Vector columnData = new Vector();
	
	String uBookName;
	String uWriter;
	String uNo;
	

	public Data(Manager window2) throws SQLException {	
		this.mw = window2;		
		setResizable(false);
		setTitle("图书馆管理系统");	
		setBounds(100, 100, 800, 550);
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		setLocationRelativeTo(getOwner());	
		setType(Type.POPUP);
		
		JPanel jp = new JPanel();	
	    jp.setOpaque(false);		
	    getContentPane().add(jp);	
	    ((JPanel) getContentPane()).setOpaque(false);	
	   // ImageIcon img = new ImageIcon(getClass().getResource("/res/mainwindow.png"));	
	    //JLabel background = new JLabel(img);	
	    //getLayeredPane().add(background, new Integer(Integer.MIN_VALUE));	
	   // background.setBounds(-75, -15, img.getIconWidth(), img.getIconHeight());
	    jp.setLayout(null);
	    
	    columnData.add("书名");
	    columnData.add("作者");
	    columnData.add("编号");
	    columnData.add("学号");
	    columnData.add("借书人");
	    columnData.add("借书状态");
	    columnData.add("预约人");
	    columnData.add("预约状态");
	    columnData.add("借书时间");
	    columnData.add("归还时间");
	    
	    rowData.addAll(dbConnMng.GetBookInfo());
	    
		final JTable table = new JTable(rowData, columnData);
		final JScrollPane scrollpane = new JScrollPane(table);
		scrollpane.setBounds(70, 140, 680, 350); 
		table.setRowHeight(50); 
	    table.setPreferredScrollableViewportSize(new Dimension(100,100));
	    jp.add(scrollpane);
	    
	    final JLabel label1 = new JLabel();
	    label1.setText("书名：");
	    label1.setBounds(80, 10, 200, 40);
	    jp.add(label1);
	    
	    final JLabel label2 = new JLabel();
	    label2.setText("作者：");
	    label2.setBounds(80, 50, 200, 40);
	    jp.add(label2);
	    
	    final JLabel label3 = new JLabel();
	    label3.setText("编号：");
	    label3.setBounds(80, 90, 200, 40);
	    jp.add(label3);
	    
	    final JTextField textfield1 = new JTextField();
	    textfield1.setBounds(120, 10, 200, 40);
	    textfield1.setText("");
	    textfield1.setBackground(new Color(248, 248, 255));
	    jp.add(textfield1);
	    
	    final JTextField textfield2 = new JTextField();
	    textfield2.setBounds(120, 50, 200, 40);
	    textfield2.setText("");
	    textfield2.setBackground(new Color(248, 248, 255));
	    jp.add(textfield2);
	    
	    final JTextField textfield3 = new JTextField();
	    textfield3.setBounds(120, 90, 200, 40);
	    textfield3.setText("");
	    textfield3.setBackground(new Color(248, 248, 255));
	    jp.add(textfield3);
	    
	    
	    
	    //JButton btnInfoMng = new JButton("学生信息管理");
	    //btnInfoMng.setBorderPainted(true);
	    //btnInfoMng.setBounds(600, 70, 112, 40);
	    //jp.add(btnInfoMng);
	    
	    JButton btnAdd = new JButton("添加");
	    btnAdd.setBorderPainted(true);
	    btnAdd.setBounds(600, 30, 140, 40);
	    btnAdd.addMouseListener(new MouseAdapter() 
	    {		
	    	@Override		
	    	public void mouseClicked(MouseEvent arg0) 
	    	{		
	    		rowData.removeAllElements();
	    		uBookName = textfield1.getText().trim();
	    		uWriter = textfield2.getText().trim();
	    		uNo = textfield3.getText().trim(); 
	    		//uNo = (String)combobox.getSelectedItem();
	    		//aclass = (String)combobox1.getSelectedItem();
	    		//abirthday = textfield4.getText().trim();
	    		//acard = textfield2.getText().trim();
	    		//abirthplace = textfield5.getText().trim();
	    		
	    		if(uNo.equals(""))
	    		{
	    			JOptionPane.showMessageDialog(null,"书号不能为空");
	    		}
	    		
	    		else if(uBookName.equals(""))
	    		{
	    			JOptionPane.showMessageDialog(null,"书名不能为空");
	    		}
	    		
	    		
	    		else
	    		{	
	    			try {
						
							try {
								dbConnMng.insertUserToDB(uBookName,uWriter,uNo);//,acard,aclass,abirthday,abirthplace);
								
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	
					} catch (HeadlessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		}
	    		
	    		 try {
					rowData.addAll(dbConnMng.GetBookInfo());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		 table.updateUI();
	    	}		
		
	    });
	    jp.add(btnAdd);
	    
	    //JButton btnStatistic = new JButton("归还图书");
	    //btnStatistic.setBorderPainted(true);
	    //btnStatistic.setBounds(600, 200, 140, 40);
	    //jp.add(btnStatistic);
	    
	    JButton btnHelp = new JButton("删除");
	    btnHelp.setBorderPainted(true);
	    btnHelp.setBounds(600, 70, 140, 40);	
	    jp.add(btnHelp);
	    
	    btnHelp.addMouseListener(new MouseAdapter() 
	    {		
	    	@Override		
	    	public void mouseClicked(MouseEvent arg0) 
	    	{	
	    		int row = table.getSelectedRow();		
	    		if(row == -1)
	    		{					
	    			JOptionPane.showMessageDialog(null,"请选择要删除的行!");	
	    		}
    		
	    		else
	    		{
	    			try {
	    				    uBookName = (String) table.getValueAt(row, 0);
	    					dbConnMng.DelBookInfo(uBookName);	
	    					
	    					rowData.removeAllElements();
	                        rowData.addAll(dbConnMng.GetBookInfo());
	    					table.updateUI();
					} catch (HeadlessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    		}
	    		
	    	}		
		
	    });
	    
	    
	    
		
		}
	}
