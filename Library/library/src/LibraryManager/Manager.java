package LibraryManager;

import java.awt.AWTEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/** * @author C5102 * * TODO To change the template for this generated type comment go to * Window - Preferences - Java - Code Style - Code Templates */

public class Manager extends JFrame{	
	/**	 * Comment for <code>serialVersionUID</code>	 */	
	private JPanel contentPanel;	
	private ClientLoginWindow clw;
	private static Manager window;
	/**	 * @param window	 */	
	public Manager(ClientLoginWindow clwIn) {	
		this.clw = clwIn;		
		setResizable(false);
		setTitle("图书馆管理系统");	
		setBounds(100, 100, 800, 550);
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		setLocationRelativeTo(getOwner());	
		setType(Type.POPUP);
		
		JPanel jp = new JPanel();	
	    jp.setOpaque(false);		
	    getContentPane().add(jp);	
	    ((JPanel) getContentPane()).setOpaque(false);	
	   // ImageIcon img = new ImageIcon(getClass().getResource("/res/mainwindow.png"));	
	    //JLabel background = new JLabel(img);	
	    //getLayeredPane().add(background, new Integer(Integer.MIN_VALUE));	
	   // background.setBounds(-75, -15, img.getIconWidth(), img.getIconHeight());
	    jp.setLayout(null);
	    
	    //JButton btnInfoMng = new JButton("学生信息管理");
	    //btnInfoMng.setBorderPainted(true);
	    //btnInfoMng.setBounds(600, 70, 112, 40);
	    //jp.add(btnInfoMng);
	    
	    JButton btnAdd = new JButton("添加图书");
	    btnAdd.setBorderPainted(true);
	    btnAdd.setBounds(600, 100, 140, 40);
	    btnAdd.addMouseListener(new MouseAdapter() 
	    {		
	    	@Override		
	    	public void mouseClicked(MouseEvent arg0) 
	    	{		
	    		Data sim = null;
				try {
					sim = new Data(window);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
				sim.setVisible(true);			
	    	}		
		
	    });	
	    jp.add(btnAdd);
	    
	    JButton btnRemove = new JButton("删除图书");
	    btnRemove.setBorderPainted(true);
	    btnRemove.setBounds(600, 200, 140, 40);
	    btnRemove.addMouseListener(new MouseAdapter() 
	    {		
	    	@Override		
	    	public void mouseClicked(MouseEvent arg0) 
	    	{		
	    		Data sim = null;
				try {
					sim = new Data(window);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
				sim.setVisible(true);			
	    	}		
		
	    });	
	    jp.add(btnRemove);
	    
	    /*JButton btnHelp = new JButton("帮助");
	    btnHelp.setBorderPainted(true);
	    btnHelp.setBounds(600, 300, 140, 40);	
	    jp.add(btnHelp);*/
	    
	    JLabel label1 = new JLabel();
	    label1.setText("姓名    ：王麻子");
	    label1.setBounds(80, 30, 200, 40);
	    jp.add(label1);
	    
	    JLabel label2 = new JLabel();
	    label2.setText("工号    ：8848");
	    label2.setBounds(80, 50, 200, 40);
	    jp.add(label2);
	    
	    //JLabel label3 = new JLabel();
	    //label3.setText("一卡通：213119999");
	    //label3.setBounds(80, 70, 200, 40);
	    //jp.add(label3);
	    
	    
		clw.frame.setVisible(false);	
		}
	}
